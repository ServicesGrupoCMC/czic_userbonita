package com.bbva.czic.userbonita.dao;

import java.util.List;

import com.bbva.czic.userbonita.business.dto.DTOIntUserbonita;
import com.bbva.czic.userbonita.facade.v01.dto.EntryBonita;

public interface UserbonitaDAO {

	public List<DTOIntUserbonita> listApplicationProfileFeedback(EntryBonita entryBonita);	

	public List<DTOIntUserbonita> getApplicationProfile(EntryBonita entryBonita);
	
}

