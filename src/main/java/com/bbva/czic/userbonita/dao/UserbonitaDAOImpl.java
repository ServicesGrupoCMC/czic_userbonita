package com.bbva.czic.userbonita.dao;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.czic.userbonita.business.dto.DTOIntUserbonita;
import com.bbva.czic.userbonita.business.dto.idm.Idm;
import com.bbva.czic.userbonita.business.dto.idm.User;
import com.bbva.czic.userbonita.facade.v01.SrvUserbonitaV01;
import com.bbva.czic.userbonita.facade.v01.dto.EntryBonita;
import com.bbva.czic.userbonita.facade.v01.mapper.Mapper;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.rest.RestConnector;
import com.bbva.jee.arq.spring.core.rest.RestConnectorResponse;

@Component()
public class UserbonitaDAOImpl  implements UserbonitaDAO {
	
	private static I18nLog log = I18nLogFactory.getLogI18n(SrvUserbonitaV01.class,"META-INF/spring/i18n/log/mensajesLog");
	
	@Autowired
	RestConnector restConn;
	
	//De
	String urlIdm = "https://82.250.88.91:9296/bbvaidm/rest/User/Lookup";
	//Qa
	//String urlIdm = "https://82.255.84.52:9296/bbvaidm/rest/User/Lookup";

	@Override
	public List<DTOIntUserbonita> listApplicationProfileFeedback(EntryBonita entryBonita) {
		RestConnectorResponse gResponse = new RestConnectorResponse();
		
		log.debug("" + entryBonita.getFeedbackDate());
		log.debug("" + entryBonita.getApp());
		log.debug("" + entryBonita.getFields());

		String payload = "{\"tclassification\": \"DATE\" , \"vclassification\": \""+ entryBonita.getFeedbackDate() 
		+"\", \"fields\": \"" + entryBonita.getFields() + "\", \"aplication\": \"" +entryBonita.getApp()+ "\",}";
		
		HashMap<String, String> optionalHeaders = new HashMap<String, String>();
		optionalHeaders.put( "Content-Type" , "application/json" );

		gResponse = restConn.doPost(urlIdm, null, optionalHeaders, payload,null);
		String json = gResponse.getResponseBody();
		
		//return null;
		
		return Mapper.userToListDTOIntUser(Mapper.jsonToListDto(json));
	}

	@Override
	public List<DTOIntUserbonita> getApplicationProfile(EntryBonita entryBonita) {
		RestConnectorResponse gResponse = new RestConnectorResponse();
		
		String payload = "{\"tclassification\": \"USER\" , \"vclassification\": \""+ entryBonita.getRegistrationIdentifier() +"\", \"fields\": \"" 
		+ entryBonita.getFields() + "\", \"aplication\": \"" +entryBonita.getApp()+ "\",}";
		
		HashMap<String, String> optionalHeaders = new HashMap<String, String>();
		optionalHeaders.put( "Content-Type" , "application/json" );
		
		gResponse = restConn.doPost(urlIdm, null, optionalHeaders, payload, null);
		String json = gResponse.getResponseBody();
		//return null;
		return Mapper.userToListDTOIntUser(Mapper.jsonToListDto(json));
	}
	
}

