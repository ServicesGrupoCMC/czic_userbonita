
package com.bbva.czic.userbonita.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "currentClassification", namespace = "urn:com:bbva:czic:userbonita:facade:v01:dto")
@XmlType(name = "currentClassification", namespace = "urn:com:bbva:czic:userbonita:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurrentClassification
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Identifier associated to current classification.", required = false)
    private String id;
    @ApiModelProperty(value = "Name of current classification", required = false)
    private String name;

    public CurrentClassification() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
