package com.bbva.czic.userbonita.facade.v01.dto;

import java.util.List;

public class SetEntry {
	
	List<UserBonita> data;

	public List<UserBonita> getData() {
		return data;
	}

	public void setData(List<UserBonita> data) {
		this.data = data;
	}	
}
