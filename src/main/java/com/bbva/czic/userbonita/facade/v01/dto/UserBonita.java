
package com.bbva.czic.userbonita.facade.v01.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "userbonita", namespace = "urn:com:bbva:czic:userbonita:facade:v01:dto")
@XmlType(name = "userbonita", namespace = "urn:com:bbva:czic:userbonita:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserBonita
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Employee identifier", required = false)
    private String employeeId;
    @ApiModelProperty(value = "BBVA identifier. This identifier is used for HR and authentication purposes.", required = false)
    private String registrationIdentifier;
    @ApiModelProperty(value = "Classification employee of a specific application.", required = false)
    private List<Classifications> classifications;
    @ApiModelProperty(value = "Classification employee of a specific application.", required = false)
    private List<AddionalTags> addionalTags;
    @ApiModelProperty(value = "Comments about the employee's profile.", required = false)
    private String comments;

    public UserBonita() {
        //default constructor
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getRegistrationIdentifier() {
        return registrationIdentifier;
    }

    public void setRegistrationIdentifier(String registrationIdentifier) {
        this.registrationIdentifier = registrationIdentifier;
    }
    
    public List<Classifications> getClassifications() {
		return classifications;
	}

	public void setClassifications(List<Classifications> classifications) {
		this.classifications = classifications;
	}

	public List<AddionalTags> getAddionalTags() {
		return addionalTags;
	}

	public void setAddionalTags(List<AddionalTags> addionalTags) {
		this.addionalTags = addionalTags;
	}

	public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

}
