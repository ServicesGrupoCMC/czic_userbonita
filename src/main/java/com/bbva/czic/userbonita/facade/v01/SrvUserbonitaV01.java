package com.bbva.czic.userbonita.facade.v01;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.jaxrs.PATCH;

import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;

import com.bbva.czic.userbonita.business.ISrvIntUserbonita;
import com.bbva.czic.userbonita.business.dto.DTOIntUserbonita;
import com.bbva.czic.userbonita.facade.v01.dto.EntryBonita;
import com.bbva.czic.userbonita.facade.v01.dto.SetEntry;
import com.bbva.czic.userbonita.facade.v01.dto.UserBonita;
import com.bbva.czic.userbonita.facade.v01.dto.Classifications;
import com.bbva.czic.userbonita.facade.v01.dto.CurrentClassification;
import com.bbva.czic.userbonita.facade.v01.dto.AddionalTags;
import com.bbva.czic.userbonita.facade.v01.mapper.Mapper;



	
@Path("/V01")
@SN(registryID="SNCO1700015",logicalID="userBonita")
@VN(vnn="V01")
@Api(value="/userbonita/V01",description="This API includes all the available services to manage information about BBVA Group employees. An employee is an individual who works under a contract of employment for BBVA Group. Outsourced personnel is not included.")
@Produces({ MediaType.APPLICATION_JSON})
@Service

	
public class SrvUserbonitaV01 implements ISrvUserbonitaV01, com.bbva.jee.arq.spring.core.servicing.utils.ContextAware {

	private static I18nLog log = I18nLogFactory.getLogI18n(SrvUserbonitaV01.class,"META-INF/spring/i18n/log/mensajesLog");

	public HttpHeaders httpHeaders;
	
	@Autowired
	BusinessServicesToolKit bussinesToolKit;
	

	public UriInfo uriInfo;
	
	@Override
	public void setUriInfo(UriInfo ui) {
		this.uriInfo = ui;		
	}

	@Override
	public void setHttpHeaders(HttpHeaders httpHeaders) {
		this.httpHeaders = httpHeaders;
	}
	
	@Autowired
	ISrvIntUserbonita srvIntUserbonita;

	
	@ApiOperation(value="Service of retreiving the information a transaction.", notes="",response=Response.class)
	@ApiResponses(value = {
		@ApiResponse(code = -1, message = "aliasGCE1"),
		@ApiResponse(code = -1, message = "aliasGCE2"),
		@ApiResponse(code = 200, message = "Found Sucessfully", response=Response.class),
		@ApiResponse(code = 500, message = "Technical Error")})
	@GET
	@Path("applicationProfile/feedback")
	@SMC(registryID="SMCCO1700035",logicalID="getApplicationProfileFeedback")
	public SetEntry listApplicationProfileFeedback(@ApiParam(value="app") @QueryParam("app") String app,
			@QueryParam(value="feedbackDate")@DefaultValue("null") String feedbackDate,
			@QueryParam(value="fields")@DefaultValue("null") String fields) {
		
		EntryBonita entryBonita = Mapper.EntryToDto(feedbackDate,null,app,fields);
		
		List<UserBonita> listUserBo = Mapper.ListIntUserToListUser(srvIntUserbonita.listApplicationProfileFeedback(entryBonita));
		SetEntry setEntry = new SetEntry();
		setEntry.setData(listUserBo);
		
		return setEntry;
	}

		
	@ApiOperation(value="Method for retrieving the related information about application news containing the employees.", notes="More text...",response=UserBonita.class)
	@ApiResponses(value = {
		@ApiResponse(code = -1, message = "aliasGCE1"),
		@ApiResponse(code = -1, message = "aliasGCE2"),
		@ApiResponse(code = 200, message = "Found Sucessfully", response=Response.class),
		@ApiResponse(code = 500, message = "Technical Error")})
	@GET
	@Path("/applicationProfile/{registrationIdentifier}")
	@SMC(registryID="SMCCO1700034",logicalID="getApplicationProfile")
	public SetEntry getApplicationProfile(@ApiParam(value="identifier param") @PathParam("registrationIdentifier") String registrationIdentifier,
			@ApiParam(value="app") @QueryParam("app") String app,
			@ApiParam(value="fields") @QueryParam("fields") String fields) {
		
		EntryBonita entryBonita = Mapper.EntryToDto(null,registrationIdentifier,app,fields);

		log.debug("get");
		
		List<UserBonita> listUserBo = Mapper.ListIntUserToListUser(srvIntUserbonita.getApplicationProfile(entryBonita));
		SetEntry setEntry = new SetEntry();
		setEntry.setData(listUserBo);
		
		return setEntry;
	}

	

}
