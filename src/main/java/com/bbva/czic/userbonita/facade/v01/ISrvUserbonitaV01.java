package com.bbva.czic.userbonita.facade.v01;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.bbva.czic.userbonita.business.dto.DTOIntUserbonita;
import com.bbva.czic.userbonita.facade.v01.dto.SetEntry;
import com.bbva.czic.userbonita.facade.v01.dto.UserBonita;
import com.bbva.czic.userbonita.facade.v01.dto.Classifications;
import com.bbva.czic.userbonita.facade.v01.dto.CurrentClassification;
import com.bbva.czic.userbonita.facade.v01.dto.AddionalTags;
import com.wordnik.swagger.annotations.ApiParam;


public interface ISrvUserbonitaV01 {
 	public SetEntry listApplicationProfileFeedback(String app,String feedbackDate,String fields);
 	public SetEntry getApplicationProfile(String idUserbonita,String app, String fields);
}