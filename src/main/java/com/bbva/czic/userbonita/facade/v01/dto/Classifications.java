
package com.bbva.czic.userbonita.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "classifications", namespace = "urn:com:bbva:czic:userbonita:facade:v01:dto")
@XmlType(name = "classifications", namespace = "urn:com:bbva:czic:userbonita:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Classifications
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Identifier associated to the classification.", required = false)
    private String id;
    @ApiModelProperty(value = "Name of the classification.", required = false)
    private String name;
    @ApiModelProperty(value = "Classification employee of a specific application.", required = false)
    private CurrentClassification currentClassification;

    public Classifications() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CurrentClassification getCurrentClassification() {
        return currentClassification;
    }

    public void setCurrentClassification(CurrentClassification currentClassification) {
        this.currentClassification = currentClassification;
    }

}
