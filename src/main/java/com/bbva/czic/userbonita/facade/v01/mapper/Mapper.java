package com.bbva.czic.userbonita.facade.v01.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.bbva.czic.userbonita.business.dto.DTOIntAddionalTags;
import com.bbva.czic.userbonita.business.dto.DTOIntClassifications;
import com.bbva.czic.userbonita.business.dto.DTOIntCurrentClassification;
import com.bbva.czic.userbonita.business.dto.DTOIntUserbonita;
import com.bbva.czic.userbonita.business.dto.idm.Idm;
import com.bbva.czic.userbonita.business.dto.idm.User;
import com.bbva.czic.userbonita.facade.v01.SrvUserbonitaV01;
import com.bbva.czic.userbonita.facade.v01.dto.EntryBonita;
import com.bbva.czic.userbonita.facade.v01.dto.UserBonita;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


	public class Mapper {
		
		private static I18nLog log = I18nLogFactory.getLogI18n(SrvUserbonitaV01.class,"META-INF/spring/i18n/log/mensajesLog");
		
		public static EntryBonita EntryToDto(String feedbackDate,String registrationIdentifier,String app, String fields) {
			EntryBonita entry = new EntryBonita();
			
			if (registrationIdentifier != null) {
				entry.setRegistrationIdentifier(registrationIdentifier);
			}
			
			if (feedbackDate != null) {
				entry.setFeedbackDate(feedbackDate);
			}
			
			if(app != null && fields != null){
				entry.setApp(app);
				entry.setFields(fields);
			}else{
				throw new BusinessServiceException("wrongParameters");
			}
			
			return entry;
		}
		
		public static UserBonita dtoIntUserToUser(DTOIntUserbonita dtoIntUser){
			UserBonita userBonita = new UserBonita();
			ObjectMapper mapper = new ObjectMapper();
			
			try {
				String jsonUser = mapper.writeValueAsString(dtoIntUser);
				userBonita = mapper.readValue(jsonUser, UserBonita.class);
			} catch (Exception e) {
				log.error("Error mapeo dtoInt to dto");
			}
			
			return userBonita;
		}
		
		public static List<UserBonita> ListIntUserToListUser(List<DTOIntUserbonita> listInt) {
			List<UserBonita> listUserbonita = new ArrayList<UserBonita>();
			
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			
			try {
				String jsonString = objectMapper.writeValueAsString(listInt);
				listUserbonita = objectMapper.readValue(jsonString,new TypeReference<List<UserBonita>>() {});
			} catch (Exception e) {
				log.error("Error mapeo dtoInt to dto");
			}
			
			return listUserbonita;	
		}
		
		public static List<DTOIntUserbonita> userToListDTOIntUser(Idm objIdm) {
			List<DTOIntUserbonita> listDtoIntUserBonita = new ArrayList<DTOIntUserbonita>();
			
			for(User user : objIdm.getUsers()){
				DTOIntUserbonita dtoIntUser = new DTOIntUserbonita();
				DTOIntClassifications dtoIntClassifi = new DTOIntClassifications();
				List<DTOIntAddionalTags> dtoListIntAddional = new ArrayList<DTOIntAddionalTags>();
				
				DTOIntAddionalTags dtoIntAddionalTags;
				
				if (user.getRoles() != null) {
					if (user.getRoles().size() > 0) {
						DTOIntClassifications objDtoIntClassifications = new DTOIntClassifications();
						DTOIntCurrentClassification objDtoIntCurrent = new DTOIntCurrentClassification();
						List<DTOIntClassifications> listDTOIntClassifications = new ArrayList<DTOIntClassifications>();
						
						objDtoIntClassifications.setId("USER_INFOMIS");
						objDtoIntCurrent.setId("USER_INFOMIS");
						objDtoIntCurrent.setName(user.getRoles().get(0).getAplication());
						
						objDtoIntClassifications.setCurrentClassification(objDtoIntCurrent);
						
						listDTOIntClassifications.add(objDtoIntClassifications);
						
						dtoIntUser.setClassifications(listDTOIntClassifications);
					}
				}
				
				dtoIntUser.setRegistrationIdentifier(user.getCodigo());
				
				HashMap<String, String> jsonNuevo=new HashMap<String, String>();
				
				if (user.getCampos() != null) {
					List<DTOIntAddionalTags> objList = new ArrayList<DTOIntAddionalTags>(); 
					DTOIntAddionalTags objDtoAdd;
					
					Iterator it = user.getCampos().entrySet().iterator();
					while (it.hasNext()) {
					    Map.Entry e = (Map.Entry)it.next();
					    
					    objDtoAdd = new DTOIntAddionalTags();
					    objDtoAdd.setId(e.getKey().toString());
					    objDtoAdd.setName(e.getValue().toString());
					    
					    objList.add(objDtoAdd);
					}
					
					dtoIntUser.setAddionalTags(objList);
				}
				
								
				listDtoIntUserBonita.add(dtoIntUser);
			}
			
			return listDtoIntUserBonita;
		}
		
		public static Idm jsonToListDto(String json) {						
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			JSONParser parser = new JSONParser();
			Idm idm = new Idm();
			
			try {
				JSONArray usuariosTem= new JSONArray();
				JSONObject jsonq = (JSONObject) parser.parse(json);
				JSONArray usuarios= new JSONArray(); 
				usuarios=(JSONArray) jsonq.get("users");

				for(int i=0;i<usuarios.size();i++){
					HashMap<String, String> hasmapCampos=new HashMap<String, String>();

					JSONObject usuario=(JSONObject) parser.parse(usuarios.get(i).toString());
					if (usuario.get("campos") != null) {
						String temp=usuario.get("campos").toString();
						temp = temp.replace("[","").replace("\"","").replace("]","");
						String[] grupo1=temp.split(",");
						
						for(int t=0;t<grupo1.length;t++){
							String[] elemento=grupo1[t].split(":");
							if (elemento.length >1) {
								hasmapCampos.put(elemento[0], elemento[1]);
							}else{
								hasmapCampos.put(elemento[0], "");
							}
						}
						
						//usuario.remove("campos");
						usuario.put("campos", hasmapCampos);
						usuariosTem.add(usuario);
					}
				}
				
				jsonq.clear();
				jsonq.put("users", usuariosTem);

				idm =  objectMapper.readValue(jsonq.toString(), Idm.class);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			return idm;
		}

		public static User jsonToDto(String json) {
			ObjectMapper mapper = new ObjectMapper();
			User dtoUser = new User();
			
			try {
				dtoUser = mapper.readValue(json,User.class);
			} catch (Exception e) {
				log.error("Error mapeo json a dto");
			}
			
			return dtoUser;
		}
		
		/*public static  DTOIntUserbonita userToDtoIntUserBonita(User dtoUser) {
			List<DTOIntUserbonita> listDtoIntUser = new ArrayList<DTOIntUserbonita>();
			
			for (User user : objDTOListUser) {
				DTOIntUserbonita dtoIntUser = new DTOIntUserbonita();
				DTOIntClassifications dtoIntClassifi = new DTOIntClassifications();
				DTOIntAddionalTags dtoIntAddionalTags = new DTOIntAddionalTags();
				
				dtoIntUser.setRegistrationIdentifier(user.getCodigo());
				
				dtoIntAddionalTags.setId("SN");
				dtoIntAddionalTags.setName(user.getCampos().get("sn"));
				dtoIntAddionalTags.setId("mail");
				dtoIntAddionalTags.setName(user.getCampos().get("mail"));
				dtoIntAddionalTags.setId("fullname");
				dtoIntAddionalTags.setName(user.getCampos().get("fullname"));
				dtoIntAddionalTags.setId("logindisabled");
				dtoIntAddionalTags.setName(user.getCampos().get("logindisabled"));
				dtoIntAddionalTags.setId("employeeType");
				dtoIntAddionalTags.setName(user.getCampos().get("employeeType"));
				
				dtoIntUser.setAddionalTags(dtoIntAddionalTags);
				listDtoIntUser.add(dtoIntUser);
			}
			
			return dtoIntUser;
		}*/
		
		/*public static void jsonToListDto(String json) {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			JSONParser parser = new JSONParser();
			HashMap<String, String> jsonNuevo=new HashMap<String, String>();
			try {
				JSONObject jsonq = (JSONObject) parser.parse(json);
				JSONArray usuarios= new JSONArray(); 
				usuarios=(JSONArray) jsonq.get("users");
				
				JSONArray usuariosTem= new JSONArray();
				for(int i=0;i<usuarios.size();i++){
					JSONObject usuario=(JSONObject) parser.parse(usuarios.get(i).toString());
					String temp=usuario.get("campos").toString();
					temp = temp.replace("[","").replace("\"","").replace("]","");
					String[] grupo1=temp.split(",");
					
					for(int t=0;t<grupo1.length;t++){
						String[] elemento=grupo1[t].split(":");
						jsonNuevo.put(elemento[0], elemento[1]);
					}
					usuario.remove("campos");
					usuario.put("campos", jsonNuevo);
					usuariosTem.add(usuario);
				}
				jsonq.clear();
				jsonq.put("users", usuariosTem);
				//System.out.println(jsonq.toString());
				Idm idm = new Idm();
				idm =  objectMapper.readValue(jsonq.toString(), Idm.class);
	           
				/*for(User in : idm.getUsers()){
					HashMap<String, String> map=in.getCampos();
					
					System.out.println("ss"+in.getCampos());
				}
				
				Iterator it = jsonNuevo.entrySet().iterator();
				while (it.hasNext()) {
				    Map.Entry e = (Map.Entry)it.next();
				    System.out.println(e.getKey() + " " + e.getValue());
				}
				
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			
	}