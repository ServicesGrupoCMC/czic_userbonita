package com.bbva.czic.userbonita.facade.v01.dto;

public class EntryBonita {
	
	private String registrationIdentifier;
	private String app;
	private String fields;
	private String feedbackDate;
	
	public String getRegistrationIdentifier() {
		return registrationIdentifier;
	}
	public void setRegistrationIdentifier(String registrationIdentifier) {
		this.registrationIdentifier = registrationIdentifier;
	}
	public String getApp() {
		return app;
	}
	public void setApp(String app) {
		this.app = app;
	}
	public String getFields() {
		return fields;
	}
	public void setFields(String fields) {
		this.fields = fields;
	}
	public String getFeedbackDate() {
		return feedbackDate;
	}
	public void setFeedbackDate(String feedbackDate) {
		this.feedbackDate = feedbackDate;
	}
}
