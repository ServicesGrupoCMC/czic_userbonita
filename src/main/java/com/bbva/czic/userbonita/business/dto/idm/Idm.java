package com.bbva.czic.userbonita.business.dto.idm;

import java.util.List;

public class Idm {
	
	public final static long serialVersionUID = 1L;
	
	private List<User> users;

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
}
