package com.bbva.czic.userbonita.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.jee.arq.spring.core.host.RegistroTransacciones;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;

import com.bbva.jee.arq.spring.core.servicing.annotations.CodeTable;
import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;

import com.bbva.czic.userbonita.business.dto.DTOIntUserbonita;
import com.bbva.czic.userbonita.business.dto.DTOIntClassifications;
import com.bbva.czic.userbonita.business.dto.DTOIntCurrentClassification;
import com.bbva.czic.userbonita.business.dto.DTOIntAddionalTags;
import com.bbva.czic.userbonita.business.ISrvIntUserbonita;
import com.bbva.czic.userbonita.dao.UserbonitaDAO;
import com.bbva.czic.userbonita.facade.v01.dto.EntryBonita;


@Service
public class SrvIntUserbonita implements ISrvIntUserbonita {
	
	private static I18nLog log = I18nLogFactory.getLogI18n(SrvIntUserbonita.class,"META-INF/spring/i18n/log/mensajesLog");

	@Autowired
	BusinessServicesToolKit bussinesToolKit;
	
	@Autowired
	UserbonitaDAO dao;
	
	@Override
	public List<DTOIntUserbonita> listApplicationProfileFeedback(EntryBonita entryBonita) {
		return dao.listApplicationProfileFeedback(entryBonita);
	}

		
	@Override
	public List<DTOIntUserbonita> getApplicationProfile(EntryBonita entryBonita) {
		return dao.getApplicationProfile(entryBonita);
	}
}
