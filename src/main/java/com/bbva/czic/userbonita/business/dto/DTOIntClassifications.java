
package com.bbva.czic.userbonita.business.dto;




public class DTOIntClassifications {

    public final static long serialVersionUID = 1L;
    private String id;
    private String name;
    private DTOIntCurrentClassification currentClassification;

    public DTOIntClassifications() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DTOIntCurrentClassification getCurrentClassification() {
        return currentClassification;
    }

    public void setCurrentClassification(DTOIntCurrentClassification currentClassification) {
        this.currentClassification = currentClassification;
    }

}
