package com.bbva.czic.userbonita.business.dto.idm;

public class Roles {
	
	public final static long serialVersionUID = 1L;
	
	private String descripcion;
	private String id;
	private String aplication;
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAplication() {
		return aplication;
	}
	public void setAplication(String aplication) {
		this.aplication = aplication;
	}
	
	
}
