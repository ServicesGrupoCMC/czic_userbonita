
package com.bbva.czic.userbonita.business.dto;

import java.util.List;




public class DTOIntUserbonita {

    public final static long serialVersionUID = 1L;
    private String employeeId;
    private String registrationIdentifier;
    private List<DTOIntClassifications> classifications;
    private List<DTOIntAddionalTags> addionalTags;
    private String comments;

    public DTOIntUserbonita() {
        //default constructor
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getRegistrationIdentifier() {
        return registrationIdentifier;
    }

    public void setRegistrationIdentifier(String registrationIdentifier) {
        this.registrationIdentifier = registrationIdentifier;
    }

    public List<DTOIntClassifications> getClassifications() {
		return classifications;
	}

	public void setClassifications(List<DTOIntClassifications> classifications) {
		this.classifications = classifications;
	}

	public List<DTOIntAddionalTags> getAddionalTags() {
		return addionalTags;
	}

	public void setAddionalTags(List<DTOIntAddionalTags> addionalTags) {
		this.addionalTags = addionalTags;
	}

	public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

}
