package com.bbva.czic.userbonita.business;

import java.util.List;
import javax.ws.rs.core.Response;


import com.bbva.czic.userbonita.business.dto.DTOIntUserbonita;
import com.bbva.czic.userbonita.business.dto.DTOIntClassifications;
import com.bbva.czic.userbonita.business.dto.DTOIntCurrentClassification;
import com.bbva.czic.userbonita.business.dto.DTOIntAddionalTags;
import com.bbva.czic.userbonita.facade.v01.dto.EntryBonita;



public interface ISrvIntUserbonita {
 	public List<DTOIntUserbonita> listApplicationProfileFeedback(EntryBonita entryBonita);

	public List<DTOIntUserbonita> getApplicationProfile(EntryBonita entryBonita);

	
}