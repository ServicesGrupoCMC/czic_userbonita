package com.bbva.czic.userbonita.business.dto.idm;

public class Campos {
	
	public final static long serialVersionUID = 1L;
	
	private String sn;
	private String mail;
	private String fullname;
	private String logindisabled;
	private String employeeType;
	
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getLogindisabled() {
		return logindisabled;
	}
	public void setLogindisabled(String logindisabled) {
		this.logindisabled = logindisabled;
	}
	public String getEmployeeType() {
		return employeeType;
	}
	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}

}
