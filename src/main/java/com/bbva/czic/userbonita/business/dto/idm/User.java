package com.bbva.czic.userbonita.business.dto.idm;

import java.util.List;
import java.util.HashMap;

public class User {
	
	public final static long serialVersionUID = 1L;
	
	private List<Roles> roles;
	private String response;
	private String codigo;
	private HashMap<String,String> campos;
	
	public List<Roles> getRoles() {
		return roles;
	}
	public void setRoles(List<Roles> roles) {
		this.roles = roles;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public HashMap<String,String> getCampos() {
		return campos;
	}
	public void setCampos(HashMap<String,String> campos) {
		this.campos = campos;
	}

}
